% Defining custom datastructures
% Not that Tree :- node/2
% The v represents the checkmark
nodelabel([_|_]).
nodelabel([_]).
nodelabel(v).

node([edge(_, _)], nodelabel(_)).
node([edge(_, _)|_], nodelabel(_)).

edge(node(_, _), _).

% We need a helper function to determine if the edge labels on a path are unique
isHittingSetTree(Tree) :-
    isHittingSetTreeHelper(Tree, []).

% Helper function, the extra argument is used to store the currently known edgelabels so that we can ensure that they are unique
isHittingSetTreeHelper( node( [                ], nodelabel(NL)  ), _ ) :-
    nodelabel(NL).

isHittingSetTreeHelper( node( [edge(X, EL)     ], nodelabel(NL)  ), ELS ) :- 
    member(EL, NL),                  % Force that the edgelabel is also part of the nodelabel
    not(member(EL, ELS)),            % Make sure that the edgelabel isn't already in the list of known labels
    append([EL], ELS, ELSS),         % Append the newly learned edgelabel to the list of known labels
    isHittingSetTreeHelper(X, ELSS), % Call the function on the only remaining child of the node
    % Also make sure the datastructure is correct
    node([edge(X, EL)], nodelabel(NL)),
    edge(X, EL),
    nodelabel(NL).

isHittingSetTreeHelper( node( [edge(H, EL) | T ], nodelabel(NL) ), ELS ) :-
    member(EL, NL),                                         % Force that the edgelabel is also part of the nodelabel
    not(member(EL, ELS)),                                   % Make sure that the edgelabel isn't already in the list of known labels
    append([EL], ELS, ELSS),                                % Append the newly learned edgelabel to the list of known labels
    isHittingSetTreeHelper(H, ELSS),                        % Call the function on a child of the node
    isHittingSetTreeHelper(node(T, nodelabel(NL)), ELS),    % Make sure we also test the other children
    % Also make sure the datastructure is correct
    node([edge(H, EL) | T ], nodelabel(NL)),
    edge(H, EL),
    nodelabel(NL).

% Datastructure contains nodelabels for possible future use
% Example tree 1, is also in the slides
tree1(Tree) :-
    Tree = node([edge(node([], nodelabel(v)), x1), edge(node([edge(node([], nodelabel(v)), x1), edge(node([], nodelabel(v)), a2), edge(node([], nodelabel(v)), o1)], nodelabel([x1, a2, o1])), x2)], nodelabel([x1, x2])).
