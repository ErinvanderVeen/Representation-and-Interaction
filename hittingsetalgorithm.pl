hittingSetAlgorithm(SD, COMP, OBS, D) :-
    % Calculate initial conflict set
    tp(SD, COMP, OBS, [], CS),
    % Create the tree
    makeTree(SD, COMP, OBS, node([], nodelabel(CS)), CS, Tree),
    % Extract diagnoses from the tree
    findDiagnoses(Tree, D2),
    % Minimize, such that we return the minimal diagnoses
    minimize(D2, D).

%--------------------- CONSTRUCT TREE ---------------------%

makeTree(SD, COMP, OBS, node(CD, nodelabel([H|T])), NL, Tree) :-
    % Process the nodelabel in the list
    subtract(COMP, [H], COMPS),
    conflictSet(SD, COMPS, OBS, CS),
    % Create child based on that COMP
    makeTree(SD, COMPS, OBS, node([], nodelabel(CS)), CS, Tree2),
    % Create edge to created child
    append([edge(Tree2, H)], CD, CDS),
    makeTree(SD, COMP, OBS, node(CDS, nodelabel(T)), NL, Tree).

makeTree(_, _, _, _, [], Tree) :-
    % If the node has no conflict set, make the label a checkmark
    Tree = node([], nodelabel(v)).

makeTree(_, _, _, node(CD, nodelabel([])), [H|T], Tree) :-
    % If there are no elements left in the conflict set, return the subtree.
    Tree = node(CD, nodelabel([H|T])).
    % In order to distinguish this case from the case where there is none left,
    % we match on [H|T] to ensure that the list is not empty.
    % We could have done this with \= []. But using patternmatching increases
    % the performance of the code.

% tp returns false when it cannot find a conflict set
% we want it to return an empty list instead
conflictSet(SD, COMP, OBS, CS) :-
    not(tp(SD, COMP, OBS, [], CS)), CS = [];
    tp(SD, COMP, OBS, [], CS).

%--------------------- FIND POSSIBLE DIAGNOSES ---------------------%

% Finds all possible diagnoses, still needs minimization
findDiagnoses(Tree, D) :-
    % The second argument stores the current parth
    findDiagnosesHelper(Tree, [], D).

findDiagnosesHelper(node([edge(C, EL)|T], NL), P, D) :-
    % For the current child, add the label to the considered path
    append([EL], P, P2),
    % Recursively call the function
    % NOTE: This is DFS, we correct this
    % By calling a minimization function later on
    findDiagnosesHelper(C, P2, D2),
    findDiagnosesHelper(node(T, NL), P, D3),
    % Combine the diagnoses from the two results
    append(D2, D3, D).

findDiagnosesHelper(node([], nodelabel([_|_])), _, D) :-
    % A node which no longer has children must return an empty list of diagnoses
    D = [].

findDiagnosesHelper(node(_, nodelabel(v)), P, D) :-
    % A leaf node must return its path as a diagnosis
    D = [P].

%--------------------- MINIMIZE GIVEN DIAGNOSES ---------------------%

removeSubsets(EL, [H], L) :-
    % In case EL is a subset OF H, remove H
    subset(EL, H),
    L = [];

    % Else, keep H
    not(subset(EL, H)),
    L = [H].

removeSubsets(EL, [H|T], L) :-
    % Recursively call on Tail of the List
    removeSubsets(EL, T, L2),
    % Such that this function doesn't clash with the one above
    T \= [],

    % In case it is a subset, remove H
    (subset(EL, H),
    L = L2;

    % Else, keep H
    not(subset(EL, H)),
    L = [H|L2]).


minimize(L, R) :-
    % The first argument keeps track of all the elements for which
    % we haven't removed the supersets
    % The second contains the list from which we remove the supersets
    minimizeHelper(L, L, R).

minimizeHelper([H|T], CL, L) :-
    % Remove all supersets
    removeSubsets(H, CL, CL2),

    % If H was originally a member, put it back
    % removing the supersets also removes the element itself
    (member(H, CL),
    CL3 = [H|CL2];

    % Else, we can just leave it out
    not(member(H, CL)),
    CL3 = CL2),

    % Continue minimizing
    minimizeHelper(T, CL3, L).

minimizeHelper([], CL, L) :-
    % Basecase, return the minimized list
    L = CL.
